const fs = require('fs');
const Scrapper = require('./lib/Scrapper');

const mongoose = require('./lib/db');
const CelebrityCats = require('./models/CelebrityCats');

const catsImagesPath = "./catsImages.csv";
const celebrityNamesPath = "./celebrityNames.csv";
const fusePath = "./fuse.csv";

fs.writeFileSync(catsImagesPath, "");
fs.writeFileSync(celebrityNamesPath, "");
fs.writeFileSync(fusePath, "");

// Scrapping of cats api

const processData = (data) => {
  return data[0].url;
}

const saveResult = (data) => {
  fs.appendFileSync(catsImagesPath, data+",");
}

for(let i =0; i < 30; i++){
  const req = Scrapper('https://api.thecatapi.com/v1/images/search', {}, processData, saveResult);
  req.end();
}

// Scrapping of celebrity names

const processData2 = ($) => {
  const data = [];
  $('.t-container strong a').each((index, nameContainer)=> {
    data.push($(nameContainer).text());
  });
  
  data.shift();data.shift();data.shift();
  return data;
}

const saveResult2 = (data) => {
  fs.writeFileSync(celebrityNamesPath, data.join(','));
}

const req2 = Scrapper('https://bigbangram.com/blog/the-top-10-most-followed-celebrities-on-instagram-in-2018', {}, processData2, saveResult2);
req2.end();

setTimeout(()=>{
  let catImages = fs.readFileSync(catsImagesPath, "utf-8");
  catImages = catImages.split(",");
  let celebrityNames = fs.readFileSync(celebrityNamesPath, "utf-8");
  celebrityNames = celebrityNames.split(",");

  let result = [];

  console.log("catImages", catImages);
  console.log("catImages", celebrityNames);

  for(let i = 0; i < celebrityNames.length; i++){
    result.push(celebrityNames[i] + ","+ catImages[i]);
  }

  fs.writeFileSync(fusePath, result.join(';'));
  
},5000);