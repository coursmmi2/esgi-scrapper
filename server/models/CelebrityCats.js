const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  Name: String,
  Image : String
}, {
  collection: "Celebrity_Cats"
});

const model = new mongoose.model('CelebrityCats', schema);

module.exports = model;